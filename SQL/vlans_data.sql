INSERT INTO vlans (ID, name, system_reserved) VALUES (0, 'System reserved VLAN', 1);
INSERT INTO vlans (ID, name, system_reserved) VALUES (1, 'Default VLAN', 1);
INSERT INTO vlans (ID, name, system_reserved) VALUES (1002, 'Token Ring Reserved', 1);
INSERT INTO vlans (ID, name, system_reserved) VALUES (1003, 'Token Ring Reserved', 1);
INSERT INTO vlans (ID, name, system_reserved) VALUES (1004, 'Token Ring Reserved', 1);
INSERT INTO vlans (ID, name, system_reserved) VALUES (1005, 'Token Ring Reserved', 1);
INSERT INTO vlans (ID, name, system_reserved) VALUES (4095, 'System reserved VLAN', 1);