create table vlans
(
	ID int not null
		primary key,
	name varchar(25) default '' not null,
	system_reserved tinyint(1) default '0' not null,
	constraint vlans_ID_uindex
		unique (ID)
)
;

create table port_roles
(
	ID int not null auto_increment
		primary key,
	name varchar(25) not null,
	deactivated tinyint(1) default '0' not null,
	constraint port_roles_ID_uindex
		unique (ID),
	constraint port_roles_name_uindex
		unique (name)
)
;

create table vlan_roles
(
	ID int not null auto_increment
		primary key,
	vlan int not null,
	role int not null,
	constraint vlan_roles_ID_uindex
		unique (ID),
	constraint vlan_roles_vlans_ID_fk
		foreign key (vlan) references vlans (ID),
	constraint vlan_roles_port_roles_ID_fk
		foreign key (role) references port_roles (ID)
)
;

create index vlan_roles_port_roles_ID_fk
	on vlan_roles (role)
;

create index vlan_roles_vlans_ID_fk
	on vlan_roles (vlan)
;


create view user_vlans as 
SELECT
    `vlans`.`ID`   AS `ID`,
    `vlans`.`name` AS `name`
  FROM `vlans`
  WHERE ((`vlans`.`system_reserved` = 0) AND (`vlans`.`ID` BETWEEN 1 AND 4095));
