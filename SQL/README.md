# SQL
In this folder you will find several files.

The file vlans.sql is the database schema I have used to keep track of my VLANS and port roles. The file vlans_data.sql are some data that you can and should insert into the table as well.

In addition I have used this [dynamic inventory](https://github.com/productsupcom/ansible-dyninv-mysql) to get the inventory into the database as well.

## What is a system reserved VLAN?
In the database schema you can see that some VLANs are marked as system reserved. By marking a VLAN as a system reserved VLAN we have the following benefits:

* An error message is given if someone tries to create a new VLAN with this ID.
* When you run a full push of configuration these VLANs are not part of the VLANs that will be removed during sync.

# Installation
This database was tested MariaDB version 5, but I expect that all newer releases of MariaDB and MySQL will work without any issues.

Create a new database for this test and run both SQL scripts on that database. Create a login that can be used to connect to the database.