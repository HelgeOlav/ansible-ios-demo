# The vlans role
This role have some Python modules that you can use to access the database. Look in the library folder to see the modules. You need to configure the database before you use these modules. See an example in the vars folder.

# Parameters
All modules need a provider argument that is shown in the vars folder. Here you specify the database connection parameters.

# Modules
The following modules exist.

## vlans_create_vlan
Creates the new VLAN in the database.

Parameters:

* provider=SQL server login data
* vlan_id=new VLAN ID
* vlan_name=name of VLAN

This module does not return anything, but fails if there are some errors.

## vlans_db
This module returns a list of user VLANS that are defined in the database.

Parameters:

* provider=SQL server login data

Returns:

* vlan_db: dict with all VLAN ID and the name of the VLAN.

## vlans_to_remove
This module will return a list of all VLANs that are not defined as a user or system reserved VLAN.

Parameters:

* provider=SQL server login data

Returns:

* vlans_removed: string array (tuple) with lists of VLANs that does not exist in range formats, like "2-4", "6-1001" etc.

## vlans_delete_vlan
Delete a VLAN from the database.

Parameters:

* provider=SQL server login data
* vlan_id=VLAN to remove

Returns changed if VLAN was removed from the database and none if no update was done. This module will fail if the VLAN is part of a role.

## vlans_from_role
Removes a VLAN from a role.

Parameters:

* provider=SQL server login data
* vlan_id=VLAN to remove
* vlan_role=name of role

Returns changed if VLAN was removed from a role.

## vlans_to_role
Adds a VLAN to a role.

Parameters:

* provider=SQL server login data
* vlan_id=VLAN to add
* vlan_role=name of role

Returns changed if success and fails if role does not exist.