#!/usr/bin/python
import simplejson
import pymysql
from ansible.module_utils.basic import AnsibleModule

def vlan_to_role(provider, myrole, myvlan):
    conn = pymysql.connect(host=provider['vlans_host'],
                           user=provider['vlans_user'],
                           password=provider['vlans_password'],
                           db=provider['vlans_db']
                           )
    try:
        sql = "insert into vlan_roles(vlan, role) values("+str(myvlan)+\
              ",(select ID from port_roles where name = '"+myrole+"'))"
        with conn.cursor() as cursor:
            cursor.execute(sql)
            conn.commit()
    finally:
        conn.close()
    return conn.affected_rows()


def main():
    module = AnsibleModule(
        argument_spec = dict(
            provider = dict(required=True),
            vlan_id = dict(required=True),
            role_name = dict(required=True)
        )
    )
    provider = simplejson.loads(module.params['provider'])
    vlan_to_role(provider, module.params['role_name'], module.params['vlan_id'])
    module.exit_json(changed=True)

if __name__ == '__main__':
    main()

