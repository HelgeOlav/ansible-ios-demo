#!/usr/bin/python
import simplejson
import pymysql
from ansible.module_utils.basic import AnsibleModule

# Create ranges from set
# set([1, 2, 4, 5, 7, 9]) => ['1-2', '4-5', '7', '9']
def simplifyset(input):
    output = []
    startval = -1 # where we started our list, -1 if we are not on a list
    lastval = -1 # last value read
    # loop through input set
    for val in input:
        # see if this is first iteration
        if lastval == -1:
            lastval = val
            startval = val
            continue
        # check if number is the next in the series and move on if so
        if val == lastval + 1:
            lastval = val
            continue
        # check if we have a range to add
        if startval != lastval:
            output.append(str(startval)+"-"+str(lastval))
            startval = val
            lastval = val
            continue
        # add single value
        output.append(str(lastval))
        lastval = val
        startval = val
    # Check if we ended a range here
    if startval != lastval:
        output.append(str(startval) + "-" + str(lastval))
    else:
        output.append(str(lastval))
    return output

def vlanstoremove(provider):
    conn = pymysql.connect(host=provider['vlans_host'],
                           user=provider['vlans_user'],
                           password=provider['vlans_password'],
                           db=provider['vlans_db']
                           )
    vlans = set();
    try:
        with conn.cursor() as cursor:
            sql = "select ID from vlans where ID between 1 and 4094"
            cursor.execute(sql)
            for result in cursor:
                vlans.add(result[0])
    finally:
        conn.close()
    ranges = set(range(1, 4095))
    return ranges.symmetric_difference(vlans)


def main():
    module = AnsibleModule(
        argument_spec = dict(
            provider = dict(required=True)
        )
    )
    provider = simplejson.loads(module.params['provider'])
    myset = vlanstoremove(provider)
    mylist = simplifyset(myset)
    response = {}
    response['vlans_removed'] = mylist
    module.exit_json(changed=False, ansible_facts=response)

if __name__ == '__main__':
    main()
