#!/usr/bin/python
import simplejson
import pymysql
from ansible.module_utils.basic import AnsibleModule

def create_vlan(provider, vlan_id, vlan_name):
    conn = pymysql.connect(host=provider['vlans_host'],
                           user=provider['vlans_user'],
                           password=provider['vlans_password'],
                           db=provider['vlans_db']
                           )
    try:
        sql = "insert into user_vlans (ID, name) values ("+str(vlan_id)+",'"+vlan_name+"')"
        with conn.cursor() as cursor:
            cursor.execute(sql)
            conn.commit()
    finally:
        conn.close()

def main():
    module = AnsibleModule(
        argument_spec = dict(
            provider = dict(required=True),
            vlan_id = dict(required=True),
            vlan_name = dict(required=True)
        )
    )
    provider = simplejson.loads(module.params['provider'])
    create_vlan(provider, module.params['vlan_id'], module.params['vlan_name'])
    module.exit_json(changed=True)

if __name__ == '__main__':
    main()
