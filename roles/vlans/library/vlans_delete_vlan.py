#!/usr/bin/python
import simplejson
import pymysql
from ansible.module_utils.basic import AnsibleModule

def delete_vlan(provider, vlan_id):
    conn = pymysql.connect(host=provider['vlans_host'],
                           user=provider['vlans_user'],
                           password=provider['vlans_password'],
                           db=provider['vlans_db']
                           )
    try:
        sql = "delete from user_vlans where ID="+str(vlan_id)
        with conn.cursor() as cursor:
            cursor.execute(sql)
            conn.commit()
    finally:
        conn.close()
    return conn.affected_rows()

def main():
    module = AnsibleModule(
        argument_spec = dict(
            provider = dict(required=True),
            vlan_id = dict(required=True)
        )
    )
    provider = simplejson.loads(module.params['provider'])
    count = delete_vlan(provider, module.params['vlan_id'])
    hasChanged = count > 0
    module.exit_json(changed=hasChanged)

if __name__ == '__main__':
    main()
