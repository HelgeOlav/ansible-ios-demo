#!/usr/bin/python
import simplejson
import pymysql
import re
from ansible.module_utils.basic import AnsibleModule

DOCUMENTATION = '''
---
module: vlans_db
short_description: Returns a list of configured VLANs from database as dict
'''

EXAMPLES = '''
- name: get VLANs from database
  vlans_db:
   provider:
    vlans_db: my_sql_database
    vlans_host: my_server
    vlans_user: my_login
    vlans_password: my_password
- name: Print VLAN database
  debug: msg="{{ vlan_db }}"
'''

def to_safe(word):
    return re.sub("[^A-Za-z0-9\-]", "_", word)

def get_vlans(provider):
    results = {}
    conn = pymysql.connect(host=provider['vlans_host'],
                           user=provider['vlans_user'],
                           password=provider['vlans_password'],
                           db=provider['vlans_db']
                           )
    try:
        with conn.cursor() as cursor:
            sql = "select ID, name from user_vlans"
            cursor.execute(sql)
            for result in cursor:
                results[result[0]]=to_safe(result[1])
    finally:
        conn.close()
    return results

def main():
    module = AnsibleModule(
        argument_spec = dict(
            provider = dict(required=True)
        )
    )
    provider = simplejson.loads(module.params['provider'])
    response = get_vlans(provider)
    vlandb = {}
    facts = {}
    vlandb['vlan_db'] = response
    facts['ansible_facts'] = vlandb
    module.exit_json(changed=False, ansible_facts=vlandb)

if __name__ == '__main__':
    main()

