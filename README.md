# Ansible IOS demo
This demo contains scripts to demonstrate the use of Ansible Tower for network provisioning of the following services:

* Creation and deletion of VLANs on all your switches.
* Port templates (setting VACL per trunk interface based on port role).
* Configuration of L3-interfaces.

## Architecture
A list of VLANs, port roles and mapping of VLANs to port roles are stored in a database. The database schema is found in the SQL directory. All access to the database is done via custom modules implemented in the vlans role.

Roles are used in most of the playbooks to reuse as much code as possible.

# Playbooks
Here is a list of the playbooks that you can run and how to use them. To specify extra parameters via the command line (when running ansible-playbook) use the option -e "var=value var2=value ...". For most of the variables you pass there are no validation of them.

## create_l3if.yml
Creates a new VLAN interface and DHCP pool.

To use this module you have to make sure that your router is in the host group ios. You can only run this playbook against one host. The L2-VLAN has to be created using create_vlan.yml for this to work.

Parameters:

* target=hostname
* subnet=0..255
* vlan_id=1..4094

The subnet variable creates a VLAN with the IP network 10.234.subnet.1/24 and a DHCP server to serve clients.

## delete_l3if.yml
Deletes a VLAN interface and matching DHCP pool.

To use this module you have to make sure that your router is in the host group ios. You can only run this playbook against one host.

Parameters:

* vlan_id=1..4094

## create_vlan.yml
This playbook creates a VLAN in the SQL database and also configures it on all switches. If the VLAN cannot be created in the SQL database the playbook will fail.

Parameters:

* vlan_id=1..4094
* vlan_name=your name, no whitespace or non US-ASCII
* target=switch or group to push to, defaults to group ios

## delete_vlan.yml
This playbook deletes a VLAN from the SQL database and remove it from the switches. This script will fail if the VLAN does not exist in the database or the VLAN is tied to any port roles.

Parameters:

* vlan_id=1..4094
* target=switch or group to push to, defaults to group ios

## ios_baseline.yml
This playbook configures a baseline for your switches.

Parameters:

* target=switch or group to push to, defaults to group ios

## reconfigure_vlans.yml
This playpool will make sure all your VLAN (from the SQL database exist on the switch) and optionally then try to remove all that should not be there.

Parameters:

* target=switch or group to push to, defaults to group ios
* vlans_allow_remove_vlans=y if you want to also remove VLANs

TODO: improve remove VLAN handling so it takes less than an hour to complete.

## vlan_from_role.yml
This playbook will remove an existing VLAN from a role in the database and then update all switches with this change.

Parameters:

* target=switch or group to push to, defaults to group ios
* vlan_id=1..4094
* role_name=name of existing role in database

TODO: finish playbook and test it

## vlan_to_role.yml
This playbook will add an existing VLAN to a role in the database and then update all switches with this change.

Parameters:

* target=switch or group to push to, defaults to group ios
* vlan_id=1..4094
* role_name=name of existing role in database

TODO: finish playbook and test it

## port_to_role.yml
Configures a port on a switch to be a specific role. Roles can be any of:

* 1..4094 VLAN ID (access port)
* shutdown - disables the port
* dyndes - configures the port as a dynamic desirable port in VLAN 1
* role_name - assign port to a given role (trunk, allowed VLANS are from the SQL database)

Parameters:

* target=switch to configure, hostgroups not allowed here
* desc=optional description
* port_role=type of port as described above
* port_name=name of interface (like gi0/1)
* desc=Port description

## create_lnx_user.yml
Creates users to use on Linux servers. It will not set users password if user already exist.

Parameters:

* username=your username
* password=your password
* target=what server to run on, default: linux

TODO: make it work!

## debug.yml
Used during development to check that all variables are correct.

## create_portroles.yml
Creates a predefined set of portroles to the switches. All roles are set no VLANs in the VACL.

# Installation
Install [Ansible](http://docs.ansible.com/ansible/intro_installation.html) as needed for your platform. You also have to install the following modules using pip or your platform package manager:

* netaddr
* pymysql
* passlib
* simplejson

There is an issue running SSH commands from Tower on CentOS. Read this [issue](https://github.com/ansible/ansible-modules-core/issues/5316) to solve that.

# Other documentation

* [Learn YAML](https://learnxinyminutes.com/docs/yaml/) - quick sheet to the YAML syntax.
* [Dynamic inventory with MySQL](https://github.com/productsupcom/ansible-dyninv-mysql) - an example on how to use MySQL as a repository to your inventory file.
* [Tips and tricks](http://jensd.be/587/linux/tips-tricks-for-ansible) for Ansible.
* The [ios_config](https://docs.ansible.com/ansible/ios_config_module.html) module from the Ansible documentation.
* Plugin for [Mikrotik](https://github.com/eden3d/ansible-mikrotik-utils) devices.
* How to write [modules](http://blog.toast38coza.me/custom-ansible-module-hello-world/) for Ansible.
* [JSON to YAML converter](https://www.json2yaml.com)